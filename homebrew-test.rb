class HomebrewTest < Formula
  desc "test"
  homepage ""
  url "https://gitlab.com/jonasgoetz01/homebrew-test/-/archive/V1.0.1/homebrew-test-V1.0.1.tar.gz"
  sha256 "def69be819119795c5c25bf841a7abcff138776d6c3d0ed766bd4229e2cbcc09"
  license ""

  def install
    bin.install "egotecworkstationinstall"
  end

  test do
    system "${bin}/egotecworkstationinstall"
  end
end
